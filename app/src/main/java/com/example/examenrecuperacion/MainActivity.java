package com.example.examenrecuperacion;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtAltura;
    private EditText txtPeso;
    private TextView txtIMC;
    private Button btnCalcular;
    private Button btnGuardar;
    private Button btnVerHistorial;
    private Button btnSalir;

    private Aplicacion app;
    private IMC nuevoIMC;


    // -----------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.instanciarComponentes();
        this.asignarMetodosClic();

        this.app = (Aplicacion) getApplication();
        this.nuevoIMC = new IMC();
    }

    private void instanciarComponentes()
    {
        this.txtAltura = findViewById(R.id.txtAltura);
        this.txtPeso = findViewById(R.id.txtPeso);
        this.txtIMC = findViewById(R.id.txtIMC);

        this.btnCalcular = findViewById(R.id.btnCalcular);
        this.btnGuardar = findViewById(R.id.btnGuardar);
        this.btnVerHistorial = findViewById(R.id.btnVerHistorial);
        this.btnSalir = findViewById(R.id.btnSalir);
    }

    private void asignarMetodosClic()
    {
        this.btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {clic_btnCalcular();}
        });
        this.btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {clic_btnGuardar();}
        });
        this.btnVerHistorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {clic_btnVerHistorial();}
        });
        this.btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {clic_btnSalir();}
        });
    }

    private boolean validarCampos()
    {
        String strAltura = this.txtAltura.getText().toString().trim();
        String strPeso = this.txtPeso.getText().toString().trim();

        return !strAltura.isEmpty() && !strPeso.isEmpty();
    }


    // -----------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------


    private void clic_btnCalcular()
    {
        if(!validarCampos()){
            Toast.makeText(this, "Ingresar todos los campos.", Toast.LENGTH_SHORT).show();
            return;
        }

        float altura = Float.parseFloat(this.txtAltura.getText().toString().trim());
        float peso = Float.parseFloat(this.txtPeso.getText().toString().trim());

        if(!(altura > 0.0f)){
            Toast.makeText(this, "Ingresar una altura valida.", Toast.LENGTH_SHORT).show();
            return;
        }
        else if(!(peso > 0.0f)){
            Toast.makeText(this, "Ingresar un peso valido.", Toast.LENGTH_SHORT).show();
            return;
        }

        this.nuevoIMC = new IMC(1, altura, peso);

        this.txtIMC.setText(Math.round(nuevoIMC.getImc() * 100.0)/100.0  + " kg/m^2");
    }

    private void clic_btnGuardar()
    {
        if(this.nuevoIMC.getId() != 1){
            Toast.makeText(this, "Favor de calcular antes un IMC.", Toast.LENGTH_SHORT).show();
            return;
        }

        app.agregarIMC(nuevoIMC);

        Toast.makeText(this, "Se agrego IMC", Toast.LENGTH_SHORT).show();

        this.txtAltura.setText("");
        this.txtPeso.setText("");

        this.nuevoIMC = new IMC();
    }

    private void clic_btnVerHistorial(){
        Intent intent = new Intent(MainActivity.this, historialActivity.class);
        startActivityForResult(intent, 0);
    }

    private void clic_btnSalir(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Salir");
        builder.setMessage("¿Estás seguro de que deseas salir?");

        builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Nada
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}