package com.example.examenrecuperacion;

import android.app.Application;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class IMCAdapter extends RecyclerView.Adapter<IMCAdapter.ViewHolder> {

    // Lista con la información de los IMCs
    private ArrayList<IMC> infIMCs;
    private Context context;
    private View.OnClickListener listener;

    public IMCAdapter(ArrayList<IMC> infIMCs, Application contexto) {
        this.infIMCs = infIMCs;
        this.context = contexto;
        this.listener = null;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textView = itemView.findViewById(android.R.id.text1);
            this.textView.setTextColor(Color.BLACK);
        }
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        IMC imc = this.infIMCs.get(position);

        String strIMC = "ID: " + imc.getId()
                + ", Altura: " + imc.getAltura()
                + " cm, Peso: " + imc.getPeso()
                + " kg, IMC: " + imc.getImc();

        holder.textView.setText(strIMC);
        if (listener != null)
            holder.textView.setOnClickListener(this.listener);
    }

    @Override
    public int getItemCount() {
        return this.infIMCs.size();
    }
}
