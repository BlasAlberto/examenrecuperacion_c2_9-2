package com.example.examenrecuperacion;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class historialActivity extends AppCompatActivity {

    private RecyclerView rvHistorial;
    private RecyclerView.LayoutManager layoutManager;
    private TextView txtPromedioIMC;
    private Button btnRegresar;
    private Button btnBorrarHistorial;


    // -----------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial);

        this.instanciarComponentes();
        this.asignarMetodosClic();

        this.txtPromedioIMC.setText(Double.toString(Math.round(Aplicacion.promedioIMC()*100.0)/100.0));
    }

    private void instanciarComponentes()
    {
        this.layoutManager = new LinearLayoutManager(this);
        this.rvHistorial = findViewById(R.id.rvHistorial);
        this.rvHistorial.setAdapter(Aplicacion.getAdaptador());
        this.rvHistorial.setLayoutManager(this.layoutManager);

        this.txtPromedioIMC = findViewById(R.id.txtPromedioIMC);

        this.btnRegresar = findViewById(R.id.btnRegresar);
        this.btnBorrarHistorial = findViewById(R.id.btnBorrarHistorial);
    }

    private void asignarMetodosClic(){
        this.btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { clic_btnRegresar(); }
        });

        this.btnBorrarHistorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { clic_btnBorrarHistorial(); }
        });
    }


    // -----------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------


    private void clic_btnRegresar(){
        finish();
    }
    private void clic_btnBorrarHistorial(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Borrar todo");
        builder.setMessage("¿Estás seguro de que deseas borrar el historial?");

        builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Aplicacion.eliminarTodo();

                int tamaño = Aplicacion.listaIMC.size();
                for (int j=tamaño-1; j>=0; j--){
                    Aplicacion.listaIMC.remove(j);
                }

                finish();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Nada
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}