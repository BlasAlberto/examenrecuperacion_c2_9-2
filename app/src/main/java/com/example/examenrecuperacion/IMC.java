package com.example.examenrecuperacion;

public class IMC {
    private int id;
    private float altura;
    private float peso;
    private float imc;


    // -----------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------


    public IMC() {
        this.id = 0;
        this.altura = 0.0f;
        this.peso = 0.0f;
        this.imc = 0.0f;
    }
    public IMC(int id, float altura, float peso) {
        this.id = id;
        this.altura = altura;
        this.peso = peso;
        this.imc = calcularIMC();
    }


    // -----------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------


    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public float getAltura() {
        return altura;
    }
    public void setAltura(float altura) {
        this.altura = altura;
    }

    public float getPeso() {
        return peso;
    }
    public void setPeso(float peso) {
        this.peso = peso;
    }

    public float getImc() {
        return imc;
    }


    // -----------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------


    private float calcularIMC() {
        if (altura <= 0 || peso <= 0) {
            return 0;
        }
        float alturaMetros = altura / 100.0f;
        return peso / (alturaMetros * alturaMetros);
    }
}
