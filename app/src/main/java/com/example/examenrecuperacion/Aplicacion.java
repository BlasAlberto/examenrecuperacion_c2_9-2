package com.example.examenrecuperacion;

import android.app.Application;

import com.example.examenrecuperacion.Modelo.IMCDB;

import java.util.ArrayList;

public class Aplicacion extends Application
{
    public static ArrayList<IMC> listaIMC;
    private static IMCDB db;
    private static IMCAdapter adaptador;


    // -----------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------


    @Override
    public void onCreate() {
        super.onCreate();

        this.db = new IMCDB(this);

        this.listaIMC = new ArrayList<>();
        this.listaIMC = db.allIMCs();

        this.adaptador = new IMCAdapter(this.listaIMC, this);
    }


    // -----------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------


    public static IMCAdapter getAdaptador() {
        return adaptador;
    }


    // -----------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------


    public static void agregarIMC(IMC nuevoIMC)
    {
        if(nuevoIMC == null)
            return;
        if (nuevoIMC.getId() == 0)
            return;

        db.insertIMC(nuevoIMC);

        nuevoIMC = db.ultimoIMC();
        listaIMC.add(nuevoIMC);
    }

    public static void eliminarTodo()
    {
        db.deleteAllIMC();
    }

    public static float promedioIMC()
    {
        float promedio = 0.0f;
        int tamañoLista = listaIMC.size();

        for(int i=0; i<tamañoLista; promedio+=listaIMC.get(i).getImc(), i++);

        return promedio/tamañoLista;
    }
}
