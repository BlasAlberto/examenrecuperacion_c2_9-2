package com.example.examenrecuperacion.Modelo;

import android.database.Cursor;

import com.example.examenrecuperacion.IMC;

import java.util.ArrayList;

public interface Proyeccion
{
    IMC getIMC(int id);
    ArrayList<IMC> allIMCs();
    IMC readIMC(Cursor cursor);
    IMC ultimoIMC();
}
