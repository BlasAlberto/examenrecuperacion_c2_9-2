package com.example.examenrecuperacion.Modelo;

import com.example.examenrecuperacion.IMC;

public interface Persistencia
{
    void openDataBase();
    void closeDataBase();
    long insertIMC(IMC imc);
    long updateIMC(IMC imc);
    void deleteIMC(int id);
}
