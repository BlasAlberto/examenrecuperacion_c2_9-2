package com.example.examenrecuperacion.Modelo;

public class DefineTabla
{
    public DefineTabla() {
    }

    public static abstract class IMC {
        public static final String TABLE_NAME = "imc";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_ALTURA = "altura";
        public static final String COLUMN_NAME_PESO = "peso";
        public static final String COLUMN_NAME_IMC = "imc";

        public static String[] REGISTRO = new String[]{
                IMC.COLUMN_NAME_ID,
                IMC.COLUMN_NAME_ALTURA,
                IMC.COLUMN_NAME_PESO,
                IMC.COLUMN_NAME_IMC
        };
    }
}
