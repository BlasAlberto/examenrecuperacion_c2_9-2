package com.example.examenrecuperacion.Modelo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class IMCDBHelper extends SQLiteOpenHelper
{
    private static final String TEXT_TYPE = " TEXT";
    private static final String CADENA_INT = " INTEGER";
    private static final String CADENA_FLOAT = " REAL";
    private static final String COMMA_SEP = ", ";

    private static final String SQL_CREATE_IMC = "CREATE TABLE " +
            DefineTabla.IMC.TABLE_NAME + "( " +
            DefineTabla.IMC.COLUMN_NAME_ID + CADENA_INT + " PRIMARY KEY" + COMMA_SEP +
            DefineTabla.IMC.COLUMN_NAME_ALTURA + CADENA_FLOAT + COMMA_SEP +
            DefineTabla.IMC.COLUMN_NAME_PESO + CADENA_FLOAT + COMMA_SEP +
            DefineTabla.IMC.COLUMN_NAME_IMC + CADENA_FLOAT + " );";

    private static final String SQL_DELETE_IMC = "DROP TABLE IF EXISTS " + DefineTabla.IMC.TABLE_NAME;

    private static final String DATABASE_NAME = "imc.db";
    private static final int DATABASE_VERSION = 1;

    public IMCDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_IMC);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_IMC);
        onCreate(sqLiteDatabase);
    }
}
