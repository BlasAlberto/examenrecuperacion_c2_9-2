package com.example.examenrecuperacion.Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.examenrecuperacion.Aplicacion;
import com.example.examenrecuperacion.IMC;

import java.util.ArrayList;

public class IMCDB implements Persistencia, Proyeccion {
    private Context context;
    private IMCDBHelper helper;
    private SQLiteDatabase db;

    public IMCDB(Context context, IMCDBHelper helper) {
        this.context = context;
        this.helper = helper;
    }

    public IMCDB(Context context) {
        this.context = context;
        this.helper = new IMCDBHelper(this.context);
        this.openDataBase();
    }

    @Override
    public void openDataBase() {
        db = helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    @Override
    public long insertIMC(IMC imc) {
        ContentValues values = new ContentValues();
        values.put(DefineTabla.IMC.COLUMN_NAME_ALTURA, imc.getAltura());
        values.put(DefineTabla.IMC.COLUMN_NAME_PESO, imc.getPeso());
        values.put(DefineTabla.IMC.COLUMN_NAME_IMC, imc.getImc());

        this.openDataBase();
        long num = db.insert(DefineTabla.IMC.TABLE_NAME, null, values);
        return num;
    }

    @Override
    public long updateIMC(IMC imc) {
        ContentValues values = new ContentValues();
        values.put(DefineTabla.IMC.COLUMN_NAME_ALTURA, imc.getAltura());
        values.put(DefineTabla.IMC.COLUMN_NAME_PESO, imc.getPeso());
        values.put(DefineTabla.IMC.COLUMN_NAME_IMC, imc.getImc());

        this.openDataBase();
        long num = db.update(
                DefineTabla.IMC.TABLE_NAME,
                values,
                DefineTabla.IMC.COLUMN_NAME_ID + " = " + imc.getId(),
                null
        );
        return num;
    }

    @Override
    public void deleteIMC(int id) {
        this.openDataBase();
        db.delete(DefineTabla.IMC.TABLE_NAME,
                DefineTabla.IMC.COLUMN_NAME_ID + "=?",
                new String[]{String.valueOf(id)});
    }

    public void deleteAllIMC(){
        this.openDataBase();
        db.delete(DefineTabla.IMC.TABLE_NAME, null, null);
    }

    @Override
    public IMC getIMC(int id) {
        db = helper.getWritableDatabase();
        Cursor cursor = db.query(
                DefineTabla.IMC.TABLE_NAME,
                DefineTabla.IMC.REGISTRO,
                DefineTabla.IMC.COLUMN_NAME_ID + "=?",
                new String[]{String.valueOf(id)},
                null, null, null);

        cursor.moveToFirst();
        IMC imc = new IMC();
        if (cursor.getCount() > 0)
            imc = readIMC(cursor);
        cursor.close();
        return imc;
    }

    @Override
    public ArrayList<IMC> allIMCs() {
        this.openDataBase();
        Cursor cursor = db.query(
                DefineTabla.IMC.TABLE_NAME,
                DefineTabla.IMC.REGISTRO,
                null, null, null, null, null);
        ArrayList<IMC> imcsList = new ArrayList<>();
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            IMC imc = readIMC(cursor);
            imcsList.add(imc);
            cursor.moveToNext();
        }
        cursor.close();
        return imcsList;
    }

    @Override
    public IMC readIMC(Cursor cursor)
    {
        int id = cursor.getInt(0);
        float altura = cursor.getFloat(1);
        float peso = cursor.getFloat(2);

        IMC imc = new IMC(id, altura, peso);

        return imc;
    }

    @Override
    public IMC ultimoIMC() {
        db = helper.getWritableDatabase();
        String query = "SELECT * FROM " + DefineTabla.IMC.TABLE_NAME +
                " ORDER BY " + DefineTabla.IMC.COLUMN_NAME_ID +
                " DESC LIMIT 1";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        IMC imc = readIMC(cursor);
        cursor.close();
        return imc;
    }
}
